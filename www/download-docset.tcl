ad_page_contract {

    Download a docset of the Procedures currently installed

} {
} 

# ---------------------------------------------------------------
# Prepare the docset
# ---------------------------------------------------------------
package require sqlite3

set encoding "utf-8"
file delete -force /tmp/ProjectOpen.docset
file mkdir /tmp/ProjectOpen.docset/Contents/Resources/Documents/

set info_plist {<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>CFBundleIdentifier</key><string>ProjectOpen</string>
    <key>CFBundleName</key><string>ProjectOpen</string>
    <key>DocSetPlatformFamily</key><string>tcl</string>
    <key>isDashDocset</key><true/>
    <key>dashIndexFilePath</key><string>contents.htm</string>
</dict>
</plist>}

set file [open "/tmp/ProjectOpen.docset/Contents/Info.plist" w]
fconfigure $file -encoding $encoding
puts $file $info_plist
flush $file
close $file


set docs_css "
body, div, p, th, td, li, dd, ul, ol, dl, dt, blockquote {
        font-family: Verdana, sans-serif;
    }
pre, code {
        font-family: 'Courier New', Courier, monospace;
    }
pre {
        background-color:  #f6fcec;
        border-top:        1px solid #6A6A6A;
        border-bottom:     1px solid #6A6A6A;
        padding:           1em;
        overflow:          auto;
    }
body {
        background-color:  #FFFFFF;
        font-size:         12px;
        line-height:       1.25;
        letter-spacing:    .2px;
        padding-left:      .5em;
    }
h1, h2, h3, h4 {
        font-family:       Georgia, serif;
        padding-left:      1em;
        margin-top:        1em;
}
h1 {
        font-size:         18px;
        color:             #11577b;
        border-bottom:     1px dotted #11577b;
        margin-top:        0px;
    }
h2 {
        font-size:         14px;
        color:             #11577b;
        background-color:  #c5dce8;
        padding-left:      1em;
        border:            1px solid #6A6A6A;
    }
h3, h4 {
        color:             #1674A4;
        background-color:  #e8f2f6;
        border-bottom:     1px dotted #11577b;
        border-top:        1px dotted #11577b;
    }
h3 {
        font-size: 12px;
    }
h4 {
        font-size: 11px;
    }
.keylist dt, .arguments dt {
        width: 20em;
        float: left;
        padding: 2px;
        border-top: 1px solid #999;
    }
.keylist dt { font-weight: bold; }
.keylist dd, .arguments dd {
        margin-left: 20em;
        padding: 2px;
        border-top: 1px solid #999;
    }
.copy {
        background-color:  #f6fcfc;
        white-space:       pre;
        font-size:         80%;
        border-top:        1px solid #6A6A6A;
        margin-top:        2em;
    }
.tablecell {
    font-size:         12px;
    padding-left:      .5em;
    padding-right:     .5em;
}
           
"
set file [open "/tmp/ProjectOpen.docset/Contents/Resources/Documents/docs.css" w]
fconfigure $file -encoding $encoding
puts $file $docs_css
flush $file
close $file

# ---------------------------------------------------------------
# Create SQLLite Libary
# ---------------------------------------------------------------

sqlite3 db1 "/tmp/ProjectOpen.docset/Contents/Resources/docSet.dsidx"
db1 eval "CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, path TEXT);"
db1 eval "CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path);"
# ---------------------------------------------------------------
# Loop through all packages and create html page
# ---------------------------------------------------------------

# create the master contents.htm

set contents_html {
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <HTML>
    <HEAD><TITLE>]project-open[ Documentation</TITLE>
    <link rel="stylesheet" href="docs.css" type="text/css" media="all">
    </HEAD>
    <BODY><H2>]project-open[</H2>
    <DL class="keylist">
}

db_multirow installed_packages installed_packages_select {
    select version_id, pretty_name, version_name, summary, description, package_key
      from apm_package_version_info
    where installed_p = 't'
      and enabled_p = 't'
  order by upper(pretty_name)
}

template::multirow foreach installed_packages {
    
    if {$summary eq ""} {set summary "<font color='red'>MISSING</font>"}
    if {$description eq ""} {set description "<font color='red'>MISSING</font>"}
    append contents_html "
    <DT><A HREF='$package_key/contents.htm'>$pretty_name ($version_name)</A></DT><DD><b>$summary</b><p />$description</DD>
    "
}

append contents_html "</DL></BODY></HTML>"

set file [open "/tmp/ProjectOpen.docset/Contents/Resources/Documents/contents.htm" w]
fconfigure $file -encoding $encoding
puts $file $contents_html
flush $file
close $file

# ---------------------------------------------------------------
# Create all package folders and content
# ---------------------------------------------------------------

template::multirow foreach installed_packages {
    file mkdir "/tmp/ProjectOpen.docset/Contents/Resources/Documents/$package_key"
    
    # Generate the overview contents.xml
    set contents_html "
        <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
        <HTML>
        <HEAD><TITLE>$pretty_name, version $version_name</TITLE>
        <link rel='stylesheet' href='../docs.css' type='text/css' media='all'>
        </HEAD>
        <BODY><H2><a href='../contents.htm'>\]project-open\[ Documentation</a> <small>&gt;</small> $pretty_name, version $version_name</H2>
        <blockquote>
        <h2>TCL Procedures for $pretty_name</h2>
        <table cellspacing='0' cellpadding='0'>
    "
    # ---------------------------------------------------------------
    # Append all procedures
    # ---------------------------------------------------------------
    set procs [list]
    set procedures($package_key) [list]
    
    # Get the list of all procedures for all files in the package
    foreach path [apm_get_package_files -package_key $package_key -file_types tcl_procs] {
        if { [nsv_exists api_proc_doc_scripts "packages/$package_key/$path"] } {
            foreach proc [nsv_get api_proc_doc_scripts "packages/$package_key/$path"] {
                lappend procs $proc
            }
        }
    }
    
    foreach proc $procs {
        array set doc_elements [nsv_get api_proc_doc $proc]
        if { !$doc_elements(public_p) } {
            continue
        }
        
        regsub -all {::} $proc {_} proc_filename
        set first_sentence [api_first_sentence [lindex $doc_elements(main) 0]]

        if {$first_sentence ne ""} {
            # Ignore automatically generated XO Calls
            if {$first_sentence ne "Automatically generated method"} {
                append contents_html "
                  <tr valign=top>
                    <td><b><a href='${proc_filename}.htm'>$proc</a></b></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>$first_sentence</td>
                  </tr>
                "
                lappend procedures($package_key) $proc
            }
        } else {
            append contents_html "
              <tr valign=top>
                <td><b>$proc</b></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><font color=red>MISSING</font></td>
              </tr>
            "
            lappend procedures($package_key) $proc
        }

    }

    append contents_html "
        </table>
        </blockquote>
        </BODY>
        </HTML>
    "
    
    set file [open "/tmp/ProjectOpen.docset/Contents/Resources/Documents/${package_key}/contents.htm" w]
    fconfigure $file -encoding $encoding
    puts $file $contents_html
    flush $file
    close $file

}

# ---------------------------------------------------------------
#  Create the individual procedure files
# ---------------------------------------------------------------

template::multirow foreach installed_packages {

    foreach proc $procedures($package_key) {

        # Check if have some documentation
        array set doc_elements [nsv_get api_proc_doc $proc]        
        set first_sentence [api_first_sentence [lindex $doc_elements(main) 0]]

        set procedure_html "
        <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
        <HTML>
        <HEAD><TITLE>$pretty_name, $proc</TITLE>
        <link rel='stylesheet' href='../docs.css' type='text/css' media='all'>
        </HEAD>
        <BODY><H2><a href='/../contents.htm'>\]project-open\[ Documentation</a> <small>&gt;</small> <a ref='contents.htm'>$pretty_name</a><small>&gt;</small>$proc</H2>
            <table width='100%'>
              <tr><td bgcolor='#eeeeee'>[api_proc_documentation -script $proc]</td></tr>
            </table>
            </BODY>
            </HTML>
        "
    
        
        regsub -all {::} $proc {_} proc_filename
    
        set file_path "${package_key}/${proc_filename}.htm"
        set file [open "/tmp/ProjectOpen.docset/Contents/Resources/Documents/${file_path}" w]
        fconfigure $file -encoding $encoding
        puts $file $procedure_html
        flush $file
        close $file
    
        # Type Detection
        if {[lindex [split $proc "::"] 0] eq "callback"} {
            set type "Callback"
        } else {
            if {[string match "*_status_*" $proc] || [string match "*_type_*" $proc]} {
                set type "Constant"
            } else {
                set type "Procedure"
            }
        }

        db1 eval "INSERT OR IGNORE INTO searchIndex(name, type, path) VALUES ('$proc', '$type', '$file_path');"
    }
}

db1 close
set zipfile /tmp/ProjectOpen.docset.zip
cd /tmp
exec zip -r $zipfile /tmp/ProjectOpen.docset

# Return the file
set outputheaders [ns_conn outputheaders]
ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"ProjectOpen.docset.zip\""
ns_returnfile 200 application/zip $zipfile

file delete -force /tmp/ProjectOpen.docset
file delete -force $zipfile